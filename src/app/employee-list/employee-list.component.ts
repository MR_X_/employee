import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../employee.model';
import { EmployeeService } from '../employee.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  @Input() employees: Employee[];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeService.getAllEmployees().subscribe(
      (res: HttpResponse<Employee[]>) => {
        this.employees = res.body;
      }
    );
  }

  editEmployee(employee: Employee) {
    employee.isEdit = false;
  }

  updateEmployee(employee: Employee) {
    this.employeeService.addEmployee(employee).subscribe(
      (res: HttpResponse<Employee>) => {
        this.employeeService.getAllEmployees().subscribe((res1: HttpResponse<Employee[]>) => {
          this.employees = res1.body;
          employee.isEdit = true;
        });
      }
    );
  }
  deleteEmployee(id: number) {
    this.employeeService.deleteEmployee(id).subscribe(
      (res: HttpResponse<Employee>) => {
        this.employeeService.getAllEmployees().subscribe((res1: HttpResponse<Employee[]>) => {
          this.employees = res1.body;
        });
      }
    );
  }
}
