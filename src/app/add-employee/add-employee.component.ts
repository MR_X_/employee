import { Component, OnInit, Output } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee.model';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employee: Employee = {
    firstName: '',
    middleName: '',
    lastName: '',
    id: 0
  };
  newEmployeeList: Employee[];
  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.employee.firstName !== '' && this.employee.lastName !== '') {
      this.employeeService.addEmployee(this.employee).subscribe(
        (res: HttpResponse<Employee>) => {
          this.employeeService.getAllEmployees().subscribe((res1: HttpResponse<Employee[]>) => {
            this.newEmployeeList = res1.body;
          });
        }
      );
    } this.employee = {
      firstName: '',
      middleName: '',
      lastName: '',
      id: 0
    };
  }

}
