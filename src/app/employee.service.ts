import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Employee } from './employee.model';


export type EntityResponseType = HttpResponse<Employee>;

@Injectable()
export class EmployeeService {

    private resourceUrl = '/employee';
    constructor(private http: HttpClient
    ) {
    }

    getAllEmployees(): Observable<HttpResponse<Employee[]>> {
        return this.http
            .get(`${this.resourceUrl}`, { observe: 'response' })
            .map((res: HttpResponse<Employee[]>) => this.convertArrayResponse(res));
    }

    addEmployee(employee: Employee): Observable<EntityResponseType> {
        return this.http
            .post(`${this.resourceUrl}`, employee, { observe: 'response' })
            .map((res: EntityResponseType) => res);
    }

    deleteEmployee(id: number): Observable<EntityResponseType> {
        return this.http
            .delete(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => res);
    }


    private convertArrayResponse(
        res: HttpResponse<Employee[]>
    ): HttpResponse<Employee[]> {
        const jsonResponse: Employee[] = res.body;
        const body: Employee[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].isEdit = true;
            body.push(jsonResponse[i]);
        }
        return res.clone({ body });
    }



}