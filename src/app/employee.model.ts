
export class Employee {

    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    isEdit?: boolean = true;

}